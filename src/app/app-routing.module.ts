import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//const recordsModule = () => import('./modules/records/records.module').then(x => x.RecordsModule);

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full'  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
