import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataJsonService {
  fichas = [];
  constructor(private http: HttpClient) { }

  getFichas(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/fichas');
  }

  autocompleteOption(): Observable<any> {
    const map2 = new Map();
    return this.fichas.length ?
      of(this.fichas) :
      this.http.get<any>('http://localhost:3000/fichas')
        .pipe(
          map(data => {
            for (const item of data) {
              if (!map2.has(item.iniciales)) {
                map2.set(item.iniciales, true);
                this.fichas.push({
                  asignacion: item.asignacion,
                  iniciales: item.iniciales
                });
              }
            }
            return this.fichas
          }))
  }
}
