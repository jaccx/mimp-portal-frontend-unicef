import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from "../../services/token-storage.service";
import { LoginService } from "../../../modules/security/services/login.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(
    private tokenStorage: TokenStorageService,
    private loginService: LoginService
  ) { }
  isLoggedIn = false;

  ngOnInit(): void {
    console.log("navbar-active");
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }

  }

  logout(): void {
    this.tokenStorage.signOut();
    this.loginService.logout();
  }

}
