import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from "./core/services/token-storage.service";
import { Router } from "@angular/router";
import { User } from "./modules/security/models/user";
import { LoginService } from "./modules/security/services/login.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isLoggedIn: boolean;
  title = 'front-unicef';
  user: User;

  constructor(
    private tokenStorageService: TokenStorageService,
    private router: Router,
    private loginService: LoginService
  ) {
    this.loginService.user.subscribe(x => this.user = x);
  }

  ngOnInit(): void{

    /*
    console.log("on-app-component");
    if (this.tokenStorageService.getToken()) {
      this.isLoggedIn = true;
      this.router.navigate(["homefull"]);
    }
    */

  }

}
