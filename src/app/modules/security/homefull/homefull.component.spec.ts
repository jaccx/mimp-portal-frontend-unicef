import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomefullComponent } from './homefull.component';

describe('HomefullComponent', () => {
  let component: HomefullComponent;
  let fixture: ComponentFixture<HomefullComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomefullComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomefullComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
