import { Component, OnInit } from '@angular/core';
import { User } from "../models/user";
import { LoginService } from "../services/login.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  user: User;
  profileAdminStr: string;

  constructor(
    private loginService: LoginService
  ) {
    this.profileAdminStr = "admin";
    this.loginService.user.subscribe(x => this.user = x);
  }

  ngOnInit(): void {

  }

}
