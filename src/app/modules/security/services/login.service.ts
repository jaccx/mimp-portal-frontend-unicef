import { Injectable } from '@angular/core';
import { Router } from "@angular/router";
//import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from "../models/user";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  usuario: User;

  constructor(
    private router: Router
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(sessionStorage.getItem('auth-user-unicef')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  login(username, password) {
    this.usuario = new User();
    this.usuario.id = "cod01";
    this.usuario.firstName = "Carmen";
    this.usuario.lastName = "Pizarro Negrón";
    this.usuario.token = "dsfggfdsgfhdgsfgdsfgdsgfjsfdsfs";
    this.usuario.password = "123456";
    this.usuario.username = "cod01";
    this.usuario.perfil = "admin";
    this.userSubject.next(this.usuario);
    return this.usuario;
  }

  logout() {
    this.userSubject.next(null);
    this.router.navigate(['/login']);
  }


}
