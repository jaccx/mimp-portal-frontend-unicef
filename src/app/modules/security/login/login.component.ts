import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule, Validators} from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { TokenStorageService } from "../../../core/services/token-storage.service";
import { LoginService } from "../services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isLoggedIn = false;

  loginForm = new FormGroup({
    usuario: new FormControl('', Validators.required),
    contrasenia: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private tokenStorage: TokenStorageService,
    private loginService: LoginService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    if (this.tokenStorage.getToken()) {
      this.router.navigate(["homefull"]);
    }
  }

  onSubmit() {
    if (this.loginForm.valid){

      this.isLoggedIn = true;

      this.tokenStorage.saveToken("jqgwdigidgasgkagjsagdsafdsafgdafgdfassaf");
      this.tokenStorage.saveUser(this.loginService.login("user","password"));

      //this.router.navigate(["homefull"]);
      const returnUrl = this.route.snapshot.queryParams['returnUrl'] || 'homefull';
      this.router.navigateByUrl(returnUrl);
    }
  }

}
