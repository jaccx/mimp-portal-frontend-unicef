import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile-routing.module';

import { ProfileAdmComponent } from './profile-adm/profile-adm.component';
import { ProfileNewComponent } from './profile-new/profile-new.component';
import { ProfileEditComponent } from './profile-edit/profile-edit.component';
import { ProfileLayoutComponent } from './profile-layout/profile-layout.component';

import { AvatarModule } from 'ngx-avatar';
import { MaterialModule } from '../material.module';

@NgModule({
  declarations: [ProfileAdmComponent, ProfileNewComponent, ProfileEditComponent, ProfileLayoutComponent],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    AvatarModule,
    MaterialModule
  ]
})
export class ProfileModule { }
