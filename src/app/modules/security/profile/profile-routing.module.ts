import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProfileAdmComponent} from "./profile-adm/profile-adm.component";
import { ProfileEditComponent } from "./profile-edit/profile-edit.component";
import { ProfileNewComponent } from "./profile-new/profile-new.component";
import { ProfileLayoutComponent } from "./profile-layout/profile-layout.component";

const routes: Routes = [
  {
    path: '', component: ProfileLayoutComponent,
    children: [
      { path: '', component: ProfileAdmComponent},
      { path: 'new', component: ProfileNewComponent },
      { path: 'edit', component: ProfileEditComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
