import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-profile-adm',
  templateUrl: './profile-adm.component.html',
  styleUrls: ['./profile-adm.component.css']
})
export class ProfileAdmComponent implements OnInit {
  public email: string = null;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  avatarClicked(event: any): void {
    this.router.navigate(["homefull"]);
  }

  new_clicked(event: any): void {
    this.router.navigate(["profile/new"]);
  }

  edit_clicked(event: any): void {
    this.router.navigate(["profile/edit"]);
  }

}
