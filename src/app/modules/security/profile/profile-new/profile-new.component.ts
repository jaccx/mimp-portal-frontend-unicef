import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-profile-new',
  templateUrl: './profile-new.component.html',
  styleUrls: ['./profile-new.component.css']
})
export class ProfileNewComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  save_clicked(event: any): void {
    alert("ok");
  }

  cancel_clicked(event: any): void {
    this.router.navigate(["profile"]);
  }

}
