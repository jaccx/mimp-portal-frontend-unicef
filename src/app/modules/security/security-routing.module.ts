import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from "./home/home.component";
import { HomefullComponent } from "./homefull/homefull.component";
import { LoginRecoveryComponent } from "./login-recovery/login-recovery.component";
import { AuthGuard } from "../../core/guards/auth.guard";

const profileModule = () => import('./profile/profile.module').then(x => x.ProfileModule);

const routes_security: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'homefull', component: HomefullComponent, canActivate: [AuthGuard]},
  { path: 'recovery', component: LoginRecoveryComponent},
  { path: 'profile', loadChildren: profileModule, canActivate: [AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forChild(routes_security)],
  exports: [RouterModule]
})
export class SecurityRoutingModule {
  static components = [
    LoginComponent,
    HomeComponent,
    HomefullComponent,
    LoginRecoveryComponent
  ]
}
