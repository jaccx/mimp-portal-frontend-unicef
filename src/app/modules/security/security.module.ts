import { NgModule } from '@angular/core';

import { SecurityRoutingModule } from './security-routing.module';
import { HomeComponent } from './home/home.component';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { HomefullComponent } from './homefull/homefull.component';
import { LoginRecoveryComponent } from './login-recovery/login-recovery.component';
import { MaterialModule } from './material.module';
import { GestionFichasComponent } from '../records/gestion-fichas/gestion-fichas.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [SecurityRoutingModule.components, GestionFichasComponent],
  imports: [
    SecurityRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    BrowserModule
  ]
})
export class SecurityModule { }
