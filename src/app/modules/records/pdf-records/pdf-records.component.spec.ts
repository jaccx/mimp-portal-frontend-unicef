import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfRecordsComponent } from './pdf-records.component';

describe('PdfRecordsComponent', () => {
  let component: PdfRecordsComponent;
  let fixture: ComponentFixture<PdfRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PdfRecordsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
