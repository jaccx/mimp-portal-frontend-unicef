import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecordsRoutingModule } from './records-routing.module';

import { RegisterRecordsComponent } from "./register-records/register-records.component";
import { PdfRecordsComponent } from "./pdf-records/pdf-records.component";
import { ReadRecordsComponent } from "./read-records/read-records.component";
import { StepFourComponent } from './register-records/step-four/step-four.component';
import { MaterialModule } from '../security/material.module';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  declarations: [RegisterRecordsComponent, PdfRecordsComponent, ReadRecordsComponent, StepFourComponent, DashboardComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RecordsRoutingModule,
    FormsModule,
    NgxChartsModule
    ]
})
export class RecordsModule { }
