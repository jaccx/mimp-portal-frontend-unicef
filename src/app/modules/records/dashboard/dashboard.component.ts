import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DateRange } from '@angular/material/datepicker';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  selectedValue: any;
  stateColor: string = "#80BD41";

  @ViewChild('chartCard', { read: ElementRef }) chartCard: ElementRef;

  resultList = [
    { id: 1, name: 'Status de llamada' },
    { id: 2, name: 'Motivo de llamada' },
    { id: 3, name: 'Uusuario vínculo con NNA' },
    { id: 4, name: 'Llamadas no vinculadas a NNA' },
    { id: 5, name: 'Usarios según sexo' }
  ];

  view: any[] = [];

  // options
  showXAxis = false;
  showYAxis = false;
  gradient = false;
  showLegend = true;
  legendTitle = '';
  showDataLabel = true;
  showXAxisLabel = true;
  xAxisLabel = '';
  showYAxisLabel = true;
  yAxisLabel = '';
  barPadding = '40';

  colorScheme = {
    domain: ['#00AEEF', '#FFC20E', '#80BD41', '#00833D', '#FF5308']
  };

  single = [
    {
      name: "Gestantes",
      value: 224
    },
    {
      name: "De  0 - 5 años",
      value: 221
    },
    {
      name: "De 6 - 11 años",
      value: 157
    },
    {
      name: "De 12 - 17 años",
      value: 56
    },
    {
      name: "No indica",
      value: 2
    }
  ];

  range: DateRange<Date>;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    let widthContent = this.chartCard.nativeElement.getBoundingClientRect().width / 2;
    this.view = [widthContent, 200];

  }

  onSelect(event) {
    console.log(event);
  }

  onResize(event) {
    this.view = [event.target.innerWidth / 1.35, 200];
  }

  onChange(event) {
    console.log(event);
  }
}
