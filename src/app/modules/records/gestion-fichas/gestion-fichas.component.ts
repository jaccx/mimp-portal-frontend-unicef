import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDatepicker, MatDatepickerInputEvent } from '@angular/material/datepicker';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import moment from 'moment';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith, switchMap } from 'rxjs/operators';
import { DataJsonService } from 'src/app/core/services/data-json.service';
@Component({
  selector: 'app-gestion-fichas',
  templateUrl: './gestion-fichas.component.html',
  styleUrls: ['./gestion-fichas.component.css']
})
export class GestionFichasComponent implements OnInit {
  fichasList = [];
  displayedColumns: string[] = ['status', 'numeroFicha', 'nombreApellido', 'fechaCreacion', 'fechaEdicion', 'celular', 'tipologia', 'asignacion', 'opciones'];
  filterValues = {
    status: '',
    numeroFicha: '',
    nombreApellido: '',
    fechaCreacion: '',
    fechaEdicion: '',
    celular: '',
    tipologia: '',
    asignacion: ''
  };
  statusItems = [
    { name: 'En progreso', color: '#00AEEF' },
    { name: 'Abierto', color: '#80BD41' },
    { name: 'En revisión', color: '#FFC20E' },
    { name: 'Cerrado', color: '#1B3593' }
  ]
  dataSourceFichas: MatTableDataSource<any>;

  // filteredOptions: Observable<any[]>;

  statusFilter = new FormControl();
  numeroFichaFilter = new FormControl();
  nombreApellidoFilter = new FormControl();
  fechaCreacionFilter = new FormControl();
  fechaEdicionFilter = new FormControl();
  celularFilter = new FormControl();
  tipologiaFilter = new FormControl();
  asignacionFilter = new FormControl();

  asignOption = []
  filteredOptions: Observable<any[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private dataJson: DataJsonService) {
    this.dataJson.getFichas().subscribe(resp => {
      this.fichasList = resp;
      this.dataSourceFichas = new MatTableDataSource(resp);
      this.dataSourceFichas.filterPredicate = this.createFilter();
      this.dataSourceFichas.paginator = this.paginator;
      this.dataSourceFichas.sort = this.sort;
    });
    this.filteredOptions = this.asignacionFilter.valueChanges.pipe(
      startWith(''),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(val => {
        return this.filter(val || '')
      })
    )
  }

  ngOnInit(): void {
    this.statusFilter.valueChanges.subscribe(status => {
      this.filterValues.status = status;
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.numeroFichaFilter.valueChanges.subscribe(numeroFicha => {
      this.filterValues.numeroFicha = numeroFicha;
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.nombreApellidoFilter.valueChanges.subscribe(nombreApellido => {
      this.filterValues.nombreApellido = nombreApellido;
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.fechaCreacionFilter.valueChanges.subscribe(fechaCreacion => {
      this.filterValues.fechaCreacion = moment(fechaCreacion).format('D/M/YYYY');
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.fechaEdicionFilter.valueChanges.subscribe(fechaEdicion => {
      this.filterValues.fechaEdicion = moment(fechaEdicion).format('D/M/YYYY');
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.celularFilter.valueChanges.subscribe(celular => {
      this.filterValues.celular = celular;
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.tipologiaFilter.valueChanges.subscribe(tipologia => {
      this.filterValues.tipologia = tipologia;
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
    this.asignacionFilter.valueChanges.subscribe(asignacion => {
      this.filterValues.asignacion = asignacion;
      this.dataSourceFichas.filter = JSON.stringify(this.filterValues);
    });
  }

  ngAfterViewInit() {

  }

  filterChange(item) {
    const filter = this.fichasList.filter(e => e.iniciales == item);
    this.dataSourceFichas = new MatTableDataSource(filter);
    this.dataSourceFichas.paginator = this.paginator;
    this.dataSourceFichas.sort = this.sort;
  }

  createFilter(): (data: any, filter: string) => boolean {
    let filterFunction = function (data, filter): boolean {
      let searchTerms = JSON.parse(filter);
      return data.status.toLowerCase().indexOf(searchTerms.status) !== -1
        && data.numeroFicha.toString().toLowerCase().indexOf(searchTerms.numeroFicha) !== -1
        && data.nombreApellido.toLowerCase().indexOf(searchTerms.nombreApellido) !== -1
        && data.fechaCreacion.toLowerCase().indexOf(searchTerms.fechaCreacion) !== -1
        && data.fechaEdicion.toString().toLowerCase().indexOf(searchTerms.fechaEdicion) !== -1
        && data.celular.toLowerCase().indexOf(searchTerms.celular) !== -1
        && data.tipologia.toLowerCase().indexOf(searchTerms.tipologia) !== -1
        && data.asignacion.toLowerCase().indexOf(searchTerms.asignacion) !== -1;
    }
    return filterFunction;
  }

  filter(val): Observable<any[]> {
    return this.dataJson.autocompleteOption()
      .pipe(
        map(response => response.filter(option => {
          return option.asignacion.toLowerCase().indexOf(val.toLowerCase()) === 0
        }))
      )
  }

  clean() {
    this.statusFilter.reset();
    this.numeroFichaFilter.reset();
    this.nombreApellidoFilter.reset();
    this.fechaCreacionFilter.reset();
    this.fechaEdicionFilter.reset();
    this.celularFilter.reset();
    this.tipologiaFilter.reset();
    this.asignacionFilter.reset();
    this.dataSourceFichas = new MatTableDataSource(this.fichasList);
    this.dataSourceFichas.paginator = this.paginator;
    this.dataSourceFichas.sort = this.sort;
  }

}