import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-register-records',
  templateUrl: './register-records.component.html',
  styleUrls: ['./register-records.component.css']
})
export class RegisterRecordsComponent implements OnInit {
  ShowOptionUno: boolean;
  ShowOptionDos: boolean;
  ShowOptionTres: boolean;
  ShowOptionCuatro: boolean;
  constructor() { }

  ngOnInit(): void {
    this.ShowOptionUno = true;
    this.ShowOptionDos = false;
    this.ShowOptionTres = false;
    this.ShowOptionCuatro = false;
  }

  PasoUno_clicked(event: any): void {
    this.ShowOptionUno = false;
    this.ShowOptionDos = true;
    let element:HTMLElement = document.getElementById('pills-dos-tab') as HTMLElement;
    element.click();

  }

  PasoDos_clicked(event: any): void {
    this.ShowOptionDos = false;
    this.ShowOptionTres = true;
    let element:HTMLElement = document.getElementById('pills-tres-tab') as HTMLElement;
    element.click();
  }

  PasoTres_clicked(event: any): void {
    this.ShowOptionTres = false;
    this.ShowOptionCuatro = true;
    let element:HTMLElement = document.getElementById('pills-cuatro-tab') as HTMLElement;
    element.click();
  }

  PasoCuatro_clicked(event: any): void {
    alert("GUARDAR");
  }

  PasoUnoAtras_clicked(event: any): void {

  }

  PasoDosAtras_clicked(event: any): void {
    this.ShowOptionUno = true;
    this.ShowOptionDos = false;
    let element:HTMLElement = document.getElementById('pills-uno-tab') as HTMLElement;
    element.click();
  }

  PasoTresAtras_clicked(event: any): void {
    this.ShowOptionDos = true;
    this.ShowOptionTres = false;
    let element:HTMLElement = document.getElementById('pills-dos-tab') as HTMLElement;
    element.click();
  }

  PasoCuatroAtras_clicked(event: any): void {
    this.ShowOptionTres = true;
    this.ShowOptionCuatro = false;
    let element:HTMLElement = document.getElementById('pills-tres-tab') as HTMLElement;
    element.click();
  }

}
