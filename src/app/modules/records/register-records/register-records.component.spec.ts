import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterRecordsComponent } from './register-records.component';

describe('RegisterRecordsComponent', () => {
  let component: RegisterRecordsComponent;
  let fixture: ComponentFixture<RegisterRecordsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterRecordsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterRecordsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
