import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-step-four',
  templateUrl: './step-four.component.html',
  styleUrls: ['./step-four.component.css']
})
export class StepFourComponent implements OnInit {

  derivacionList: Array<any> = [
    { name: "UPE Lima", value: "", active: false, checked: false },
    { name: "UPE Lima Este", value: "", active: false, checked: false },
    { name: "UPE Lima Norte", value: "", active: false, checked: false },
    { name: "UPE Lima Sur", value: "", active: false, checked: false },
    { name: "UPE Sede Provincia", value: "", active: false, checked: false }
  ];
  orientacionList: Array<any> = [
    { name: "UPE Lima", value: "", active: false, checked: false },
    { name: "UPE Lima Este", value: "", active: false, checked: false },
    { name: "UPE Lima Norte", value: "", active: false, checked: false },
    { name: "UPE Lima Sur", value: "", active: false, checked: false },
    { name: "UPE Sede Provincia", value: "", active: false, checked: false }
  ];
  coordinacionUPEList: Array<any> = [
    { name: "UPE Lima", value: "", active: false, checked: false },
    { name: "UPE Lima Este", value: "", active: false, checked: false },
    { name: "UPE Lima Norte", value: "", active: false, checked: false },
    { name: "UPE Lima Sur", value: "", active: false, checked: false },
    { name: "UPE Sede Provincia", value: "", active: false, checked: false }
  ];
  coordinacionDPEList: Array<any> = [
    { name: "Información F.A", value: "", active: false, checked: false },
    { name: "Casos de Adopción", value: "", active: false, checked: false },
    { name: "Casos Especiales", value: "", active: false, checked: false },
  ];
  redesApoyoList: Array<any> = [
    { name: "Comisaria", value: "", active: false, checked: false },
    { name: "CSMC", value: "", active: false, checked: false },
    { name: "CEM", value: "", active: false, checked: false },
    { name: "INABIF", value: "", active: false, checked: false },
    { name: "DEMUNA", value: "", active: false, checked: false },
    { name: "Línea 104", value: "", active: false, checked: false },
    { name: "CEDIF", value: "", active: false, checked: false },
    { name: "Defensoría del Pueblo", value: "", active: false, checked: false },
    { name: "Fiscalía", value: "", active: false, checked: false },
    { name: "DGA", value: "", active: false, checked: false },
    { name: "ALEGRA", value: "", active: false, checked: false },
    { name: "DIPAM", value: "", active: false, checked: false },
    { name: "E.Salud", value: "", active: false, checked: false },
    { name: "Línea 1818", value: "", active: false, checked: false },
    { name: "Habla Franco", value: "", active: false, checked: false },
    { name: "CONADIS/OMAPED", value: "", active: false, checked: false },
    { name: "Poder Judicial", value: "", active: false, checked: false },
    { name: "Otros", value: "", active: false, checked: false },
    { name: "Línea 100", value: "", active: false, checked: false },
    { name: "Sin derivar", value: "", active: false, checked: false },
  ];

  newItemDerivacion: boolean = false;
  newItemOrientacion: boolean = false;
  newItemCoordinacionUPE: boolean = false;
  newItemCoordinacionDPE: boolean = false;
  newItemRedes: boolean = false;
  newDerivacion = "";
  newOrientacion = "";
  newCoordinacionUPE = "";
  newCoordinacionDPE = "";
  newRedes = "";

  constructor() { }

  ngOnInit(): void {

  }

  eventNew(type) {
    switch (type) {
      case "Derivacion":
        this.newItemDerivacion = !this.newItemDerivacion;
        this.newDerivacion = "";
        break;
      case "Orientacion":
        this.newItemOrientacion = !this.newItemOrientacion;
        this.newOrientacion = "";
        break;
      case "CoordinacionUPE":
        this.newItemCoordinacionUPE = !this.newItemCoordinacionUPE;
        this.newCoordinacionUPE = "";
        break;
      case "CoordinacionDPE":
        this.newItemCoordinacionDPE = !this.newItemCoordinacionDPE;
        this.newCoordinacionDPE = "";
        break;
      case "Redes":
        this.newItemRedes = !this.newItemRedes;
        this.newRedes = "";
        break;
    }
  }

  newElement(lista, input, type) {
    if (input == "") {
      this.eventNew(type);
    } else {
      lista.push({ name: input, value: "", active: false, checked: false });
      this.eventNew(type);
    }
  }

  openEdit(lista, item) {
    lista.find(e => {
      if (e == item) {
        e.active = true;
      }
    })
  }

  editElement(lista, item) {
    lista.find(e => {
      if (e.name == item.name) {
        if (item.value == "") {
          item.active = false;
        } else {
          e.name = item.value;
          e.active = false;
        }
      }
    });
  }
  
  selectItem(e){
    console.log('seleccionado', e.name);
  }
}

