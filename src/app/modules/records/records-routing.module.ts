import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from "../../core/guards/auth.guard";
import { RegisterRecordsComponent } from "./register-records/register-records.component";
import { PdfRecordsComponent } from "./pdf-records/pdf-records.component";
import { ReadRecordsComponent } from "./read-records/read-records.component";
import { GestionFichasComponent } from './gestion-fichas/gestion-fichas.component';
import { StepFourComponent } from './register-records/step-four/step-four.component';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  { path: 'records/register', component: RegisterRecordsComponent, canActivate: [AuthGuard] },
  { path: 'records/register/step-four', component: StepFourComponent, canActivate: [AuthGuard] },
  // { path: 'records/managment', component: GestionFichasComponent, canActivate: [AuthGuard] },
  { path: 'records/managment', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'records/read', component: PdfRecordsComponent, canActivate: [AuthGuard] },
  { path: 'records/pdf', component: ReadRecordsComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordsRoutingModule { }
